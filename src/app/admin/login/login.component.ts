import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./css/login.component.css',
  './css/bootstrap.min.css',
  './css/font-awesome.min.css',
  './css/ionicons.min.css',
  './css/AdminLTE.css']
})
export class LoginComponent implements OnInit {

  constructor() { 
  	console.log("LoginComponent Loaded");
  }

  ngOnInit() {
  }

}
