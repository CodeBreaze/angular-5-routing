import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { HeaderService } from '../services/header/header.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit  {
  results: {};
  amount : number;
  constructor(
    private http: HttpClient,
    private router:Router,
    private headerService:HeaderService
    ){ 
      console.log("PostsComponent Loaded");
  }

  ngOnInit() {
  	this.http.get('https://jsonplaceholder.typicode.com/posts/').subscribe((data)=>{
  		this.results = data;
      this.amount = 7954;
  	});
  }

  getDetails(id,userId){
  	this.router.navigate(['posts/',id,userId]);
  }	

}
