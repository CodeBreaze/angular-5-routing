import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})
export class PostDetailComponent implements OnInit {
  body: any;
  recent: {};
  constructor(
  	private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient,
    ) { }

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id');
  	let userId = this.route.snapshot.paramMap.get('userid');
    this.getUserPosts(userId);
  	this.http.get<UserResponse>('https://jsonplaceholder.typicode.com/posts/'+id)
    .subscribe((data)=>{
  		this.body = data.body;
  	});
  }

  getUserPosts(userId){
    this.http.get('https://jsonplaceholder.typicode.com/posts?userId='+userId).subscribe((data)=>{
      this.recent = data;
    });
  }
}

interface UserResponse {
  body: string;
}

