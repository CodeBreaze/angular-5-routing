import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './/app-routing.module';
import { RouterModule,Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

// SERVICES
import { HeaderService } from './frondend/services/header/header.service';
// SERVICES
import { InjectorComponent } from './injector/injector.component';

import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { CustomersComponent } from './admin/customers/customers.component';
import { LoginComponent } from './admin/login/login.component';

import { IndexComponent } from './frondend/index/index.component';
import { AboutComponent } from './frondend/about/about.component';
import { HeaderComponent } from './frondend/includes/header/header.component';
import { NavigationComponent } from './frondend/includes/header/navigation/navigation.component';
import { FooterComponent } from './frondend/includes/footer/footer.component';
import { ContactComponent } from './frondend/contact/contact.component';
import { PostsComponent } from './frondend/posts/posts.component';
import { PostDetailComponent } from './frondend/posts/post-detail/post-detail.component';


const routes: Routes = [
    { path: '', component: IndexComponent },
    { path: 'about', component: AboutComponent },
    { path: 'contact', component: ContactComponent },
    { path: 'posts', component: PostsComponent },
	  { path: 'posts/:id/:userid', component: PostDetailComponent },
	  { path: 'admin',children:[
	  		{ path: 'login', component: LoginComponent },
	  ]}
	];

@NgModule({
  declarations: [
    DashboardComponent,
    CustomersComponent,
    LoginComponent,
    IndexComponent,
    AboutComponent,
    InjectorComponent,
    HeaderComponent,
    NavigationComponent,
    FooterComponent,
    ContactComponent,
    PostsComponent,
    PostDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    HttpClientModule  
  ],
  providers: [HeaderService],
  bootstrap: [InjectorComponent]
})
export class AppModule { }
